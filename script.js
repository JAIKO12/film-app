$('.search-btn').on('click', function(){
    $.ajax({
        url: 'http://www.omdbapi.com/?apikey=6fc9b8aa&s='+$('.input-keyword').val(),
        
        success: results => {
            const movies = results.Search;
            let cards = ' ';
            movies.forEach( movie => {
                cards += showCards (movie);
            });
            $('.movie-container').html(cards);  
        
        
            // ketika tombol detail di click
            $('.modal-detail-btn').on('click', function() {
               $.ajax({
                   url:'http://www.omdbapi.com/?apikey=6fc9b8aa&i=' + $(this).data('imdbid'),
                   success: movie => {
                       const movieDetails = showMovieDetails(movie)
                $('.modal-body').html(movieDetails);                      
                   },
                   error: (e) => {
                    console.log(e.responseText);
                }
               })
            })
        },
        
        error: (e) => {
            console.log(e.responseText);
        }
        
        });
});




function showCards(movie) {
   return `<div class="col-md-4 my-3">
            <div class="card">
            <img src=${movie.Poster} class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${movie.Title}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${movie.Year}</h6>
                <a href="#" class="btn btn-primary modal-detail-btn" data-toggle="modal" data-target="#movieDetailModal" data-imdbid="${movie.imdbID}">Show Details</a>
            </div>
          </div>
    </div>` 
}

function showMovieDetails(movie) {
    return `<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
        <img src="${movie.Poster}" class="img-fluid">
        </div>
        <div class="col-md">
        <ul class="list-group">
            <li class="list-group-item"><h4>${movie.Title} ${movie.Year}</h4></li>
            <li class="list-group-item"><strong>Director : ${movie.Director}</strong></li>
            <li class="list-group-item"><strong>Actors : ${movie.Actors}</strong></li>
            <li class="list-group-item"><strong>Genre :${movie.Genre}  </strong></li>
            <li class="list-group-item"><strong>Plot : ${movie.Plot} </strong> <br> </li>
        </ul>
        </div>
     </div>
</div>`;
}